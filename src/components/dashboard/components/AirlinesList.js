// ****************************************************************************
// ***************** Component : Airline List        **************************
// ***************** Developer : Vikas Jagdale (07/05/2019)  ******************

import React,{Component} from 'react';
import MUIDataTable from 'mui-datatables';
import axios from 'axios';
import {getKeys} from '../../../api/keys';
import {
    createMuiTheme,
    MuiThemeProvider
  } from "@material-ui/core/styles";


class AirlinesList extends Component{
    state={
        airlineData : [],
    }

    // A life cycle hook that is use to make subscribe call
    componentDidMount(){
        var key = getKeys();
        const self = this;
        axios.get('https://aviation-edge.com/v2/public/airlineDatabase?key='+key)
            .then(function (response) {
                // console.log(response.data);
                self.setState({
                    airlineData : response.data
                })
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    getMuiTheme = () =>
    createMuiTheme({
      overrides: {
        MUIDataTable: {
          paper: {
            boxShadow: "none",
          }
        },
        MUIDataTableBodyCell: {
          root: {
              fontSize:'14px'
          }
        }
      }
    });

    // render method is important in life cycle
    render(){
        const columns = ["airlineId","Airline Name", "Founding", "Country", "Airline Size","Airline status"];
        const options = {
            filterType: 'checkbox',
            filter: false,
            // responsive: "scroll"
        };
       
        return(
            <div className="col-lg-12">
                <div className="col-lg-12 col-md-12 pros-datatable metric-alert-wrap">
                    <MuiThemeProvider theme={this.getMuiTheme()}>
                        <MUIDataTable
                                title={"List of Airlines"}
                                data={this.state.airlineData.map(data => {
                                    return [
                                        data.airlineId,
                                        data.nameAirline,
                                        data.founding,
                                        data.nameCountry,
                                        data.sizeAirline,
                                        data.statusAirline,
                                    ]
                                })}
                                columns={columns}
                                options={options}
                        />
                    </MuiThemeProvider>
                </div>
            </div>
        );
    }
}
export default AirlinesList;